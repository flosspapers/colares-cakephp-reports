# Presentation

The process of developing [free software](http://www.gnu.org/philosophy/free-sw.html)
(also known as "open source software") projects has special characteristics
that have fostered the emergence of many techniques and practices, each of
which resolves various forces and may entail many tradeoffs. In this book, we
document these practices by means of patterns.

This book is a open project, and you can [contribute](link://contribute) to it.

The Free Software Patterns are organized around clusters. Each cluster of
patterns is presented as a simple pattern language, that is, a set of related
patterns that may be combined to address a common set of problems. Our three
clusters of patterns are:

* **Selection Patterns** help prospective contributors to find suitable
  projects.
* **Involvement Patterns** deal with the tough question "ok, but where do I
  start?"
* **Contribution Patterns** document best practices for submitting your
  contribution to a free software project.

The *Selection Patterns* and *Contribution Patterns* clusters are intrinsically
associated with the nature of free software projects, and represent novel work
by us.

Getting involved with a free software project, however, presents several
similarities with the process of getting involved with any software project,
such as investigating the existing documentation, building the system from its
sources, reading the code, etc. For that reason, the *Involvement Patterns*
cluster reuses several patterns from the excellent [Object-Oriented
Reengineering Patterns](http://scg.unibe.ch/download/oorp/), by Serge Demeyer,
Stéphane Ducasse and Oscar Nierstrasz, together with a few new patterns
proposed by us. In general, *Reengineering Patterns* is a recommended companion
to *Free Software Patterns*. We are grateful to Demeyer, Ducasse and Nierstrasz
for having [licensed](link://copyright) their work under a Creative Commons
license, allowing us to partially base our own work on theirs.

# Table of Contents

* [Copyright](link://copyright)
* [Contribution Guide](link://contribute)
* Selection Patterns
    * [Explore a Brave New World](link://selection/ExploreABraveNewWorld)
    * [Look Inside Your Toy Box](link://selection/LookInsideYourToyBox)
    * [Walk On Familiar Ground](link://selection/WalkOnFamiliarGround)
* Involvement Patterns
    * [First Contact](link://involvement/FirstContact) from Reengineering
      Patterns
        * [Chat with the Maintainers](link://involvement/ChatWithMaintainers)
        * [Do a Mock Installation](link://involvement/DoAMockInstallation)
        * [Interview During Demo](link://involvement/InterviewDuringDemo)
        * [Read all the Code in One Hour](link://involvement/ReadTheCodeInOneHour)
        * [Skim the Documentation](link://involvement/SkimTheDocumentation)
    * [Look for TODO Lists](link://involvement/LookForTodoLists)
    * [Easy Tasks First](link://involvement/EasyTasksFirst)
* Contribution Patterns
    * [Write Useful Bug Reports](link://contribution/WriteUsefulBugReports)
    * [Review Recent Activity](link://contribution/ReviewRecentActivity)
    * [Right Version for the Task](link://contribution/RightVersionForTheTask)
    * [Explanatory Commit Messages](link://contribution/ExplanatoryCommitMessages)
    * [Review Your Changes](link://contribution/ReviewYourChanges)
    * [Document Your Changes](link://contribution/DocumentYourChanges)
    * [Write Documentation](link://contribution/WriteDocumentation)
    * [Create a Patch](link://contribution/CreatePatch)
    * [Group Related Changes](link://contribution/GroupRelatedChanges)
    * [Separate Unrelated Changes](link://contribution/SeparateUnrelatedChanges)
    * [Manual Testing](link://contribution/ManualTesting)
    * [Add Tests that Fail](link://contribution/AddTestsThatFail)
