PATTERNS = $(shell find . -name '*.markdown')

PREPROCESSED_PATTERNS = $(patsubst %, public/%.in, $(PATTERNS))
HTML = $(patsubst %.markdown, public/%.html, $(PATTERNS))
ASSETS = $(patsubst template/%, public/%, $(wildcard template/*.css template/*.png template/*.jpg template/images/*))

all: $(HTML) $(ASSETS)

public/%: template/%
	mkdir -p $$(dirname $@)
	ln $< $@

%.html.in: %.markdown.in
	pandoc --smart -f markdown -t html -o $@ $<

%.html: %.html.in ./script/apply-template.rb template/layout.html
	ruby ./script/apply-template.rb template/layout.html $< > $@

public/%.markdown.in: %.markdown script/expand-links.sed
	mkdir -p $$(dirname $@)
	sed -f script/expand-links.sed $< > $@

upload: all
	sh script/upload.sh

clean:
	$(RM) -r public
