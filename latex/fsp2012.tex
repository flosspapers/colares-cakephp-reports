\documentclass[12pt]{article}

\usepackage{sbc-template}

\usepackage{graphicx,url}

%\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\sloppy

\title{Free Software Patterns}

\author{Antonio Terceiro, Rodrigo Souza, Christina Chavez}

\address{Departamento de Ci\^{e}ncia da Computa\c{c}\~{a}o\\
  Universidade Federal da Bahia (UFBA) --Salvador, BA -- Brazil
  \email{\{terceiro,rodrigo,flach\}@dcc.ufba.br}
}

\begin{document}

\maketitle

\begin{abstract}
The process of developing free software (also known as open source software) projects has special characteristics that have fostered the emergence of many practices, each of which resolves various forces and may entail tradeoffs. The \textit{Free Software Patterns} aim at documenting such techniques and practices by means of patterns, organized around three main clusters: (a) \textit{Selection Patterns}, that help prospective contributors to find suitable projects, (b) \textit{Involvement Patterns}, that deal with the first steps towards getting familiar with the selected project, and (c) \textit{Contribution Patterns}, that document best practices for submitting different kinds of contribution to a free software project. The \textit{Free Software Patterns} catalog is itself a free software project. Its license allows free reuse of the text, as long as the modified versions are distributed under the same license.
\end{abstract}

\section{Introduction}

Free/Libre/Open Source Software (FLOSS) is a class of software projects
that have Internet-based interaction between developers and public
source code licensed under terms that comply with either the Free
Software definition by the Free Software Foundation
(FSF)\footnote{http://www.gnu.org/philosophy/free-sw.html} or the Open
Source Definition by the Open Source Initiative
(OSI)\footnote{http://www.opensource.org/docs/definition.html}.
%

A FLOSS project starts when an individual developer, or an organization,
decides to make a software product publicly available on the Internet so that
it can be freely used, modified, and redistributed.
After an initial version of a FLOSS project is released and advertised in 
the appropriate communication channels,  
it may be subject to different kinds of contributions
%
-- for instance, new features, bug fixes, documentation --
in which standard techniques and practices may be used, each
of which resolving various forces and possibly entailing many trade-offs.

Patterns are particularly well-suited to presenting and discussing
techniques and practices. For instance, the Reengineering Patterns
\cite{demeyer2008}  present solutions for  recurring software
reengineering problems. Each reengineering pattern describes one part of
the reengineering process and produces artifacts that may be as concrete
as refactored code, or in the case of reverse engineering patterns, as
abstract as insights into how the system functions \cite{demeyer2008}.

% In this work, we document practices related to contributing to FLOSS
% projects by means of patterns.
\textit{Free Software Patterns} have been inspired on the Reengineering Patterns.
They present solutions for recurring problems that emerge when
prospective contributors are willing to select a project, get familiar
with its code and related artifacts and, contribute to it. The patterns
entail activities that go beyond programming or reporting bugs.
Therefore, \textit{Free Software Patterns} also describe alternative ways of
contributing to FLOSS projects, such as documenting, performing
translations and writing tests.

The \textit{Free Software Patterns} are organized around clusters.  
Each cluster of patterns is presented as a simple pattern language, conceived to
document and to address a common set of problems.
The three clusters of patterns are:

\begin{itemize}
  \item
    \emph{\textit{Selection Patterns}} help prospective contributors to find
    suitable projects.
  \item
    \emph{\textit{Involvement Patterns}} deal with the tough question ``ok, but
    where do I start?''
  \item
    \emph{\textit{Contribution Patterns}} document best practices for
    submitting a contribution to a FLOSS project.  Patterns for
    contributing are organized according to the type of contribution,
    that is, documenting, translating, reporting bugs, resolving bugs,
    adding features, and so on.
\end{itemize}

The \textit{Selection Patterns} and \textit{Contribution Patterns} clusters are
intrinsically associated with the nature of FLOSS projects, and
represent original work. The \textit{Involvement Patterns}, however, include
practices that present several similarities with the process of getting
involved and familiar with any software project, such as investigating existing
documentation, building the system from its source, reading the code,
etc. For that reason, the \textit{Involvement Patterns} cluster reuses several
patterns from the Object-Oriented Reengineering Patterns (OORP) book, by Serge
Demeyer, Stéphane Ducasse and Oscar Nierstrasz\cite{demeyer2008}, 
together with two new patterns documented by us. 
In general, Reengineering Patterns is a recommended companion 
to Free Software Patterns. 

\subsection{Format}

Each following section describes one cluster of patterns. 
Each cluster is described in terms of the forces that influence its set of patterns, 
an overview and a map of the patterns suggesting how they may be related.  
This approach to pattern description has been inspired by the OORP book~\cite{demeyer2008}.
It includes a subset of the fields used for pattern documentation in the OORP book such as: 
pattern name, intent, problem, solution, trade-offs, rationale, known uses, related patterns
and next steps.

%-----------------------------------------------------------------
\section{\textit{Selection Patterns}}

You want to contribute to a FLOSS project.  
You know that there are several repositories that host FLOSS projects, for instance, 
sourceforge\footnote{http://sourceforge.net/} and ohloh\footnote{http://www.ohloh.net/}.
You navigate through some projects in these repositories but still have no
clue about which FLOSS project could be a good starting point.
You feel somewhat lost and concerned: there are several interesting projects, 
that use different technologies and programming languages, possibly with different 
development processes and types of participation. 
%You will probably be tempted to select a FLOSS project that is familiar to you, 
%possibly one that you actually use. 
%But, is this the main criteria that should guide the selection of a FLOSS project? 
You also know that, despite your will to contribute, in practice
you want to learn new things and work with a FLOSS project that keeps you motivated.
Are there strategies for selecting a project, aligned with
your motivation to contribute, that should be considered?
%
How to select a FLOSS project in which you can contribute and 
be encouraged to keep on contributing?

%Inserting section for FORCES - TO BE IMPROVED
\subsection{Forces}

\begin{itemize}
\item \textit{Previous use of a FLOSS project matters.} 
Previous use of a FLOSS project tends to facilitate your work and increase your motivation to contribute.
You may be aware of existing bugs and
enhancements that are necessary. This may ease your work as contributor.

\item \textit{The cuckoo in the nest syndrome.}
The contribution to a FLOSS project implies in social interaction with other contributors in a new environment, 
and -- why not? -- reputation-related issues and concerns. 
You will possibly provide better contributions for projects that use
languages and technologies you are familiar with.

\item \textit{Eager to learn}. Usually, learning is a driving force for new undertakings. 
Contributing to a FLOSS project may bring several opportunities for learning: from new technologies to new development environments and processes.
\end{itemize}

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{figures/selection}
  \end{center}
  \caption{Selection Patterns}
  \label{fig:selection}
\end{figure}

Selection Patterns help prospective contributors to find suitable FLOSS projects, depending on some criteria. 
Figure~\ref{fig:selection} presents 3 patterns that comprise this cluster.

%Explaining Figure 1 - Pattern overview. TO BE IMPROVED
You should \textit{Explore a Brave New World} whenever you are willing to choose a project based on the functionalities it provides, despite the risk of having to deal with unknown technologies. 
Or maybe you prefer to \textit{Walk on Familiar Ground} and choose a project based on the technologies that stem as a familiar ground for you. 
Yet another possible path is to \textit{Look Inside your Toolbox} and select  an open source project that you already use.

\input{../selection/ExploreABraveNewWorld.ltx}
\input{../selection/WalkOnFamiliarGround.ltx}
\input{../selection/LookInsideYourToyBox.ltx}

%----------------------------------------
\section{\textit{Involvement Patterns}}

% New preamble. TO BE IMPROVED.
You have selected an open source project to contribute with, maybe one that you already use. But it is a big project, with several thousands of lines of code, dozens of developers, bug track systems with lots of bugs to be fixed, and so on. You want to contribute with the project but first you need to get familiar with the project ecossytem.
Are there any strategies for assessing a FLOSS project that should be considered?
Where should you start?
All the patterns in this cluster can be applied  when you are facing a FLOSS project 
that is completely new for you. 

\subsection{Forces}
\begin{itemize}
\item Legacy systems are often large and complex.
\item ...
\end{itemize}


\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=1\textwidth]{figures/involvement}
  \end{center}
  \caption{\textit{Involvement Patterns}}
  \label{fig:involvement}
\end{figure}

Involvement Patterns deal with the first steps towards getting familiar with the selected project.
Figure~\ref{fig:involvement} presents the patterns that comprise this cluster.

%Explaining Figure 2 - Pattern overview. TO BE IMPROVED

\textit{First Contact} \cite{demeyer2008} consists of a set of patterns that may be useful when
you encounter a legacy system for the first time.  
FLOSS projects are legacy systems, and therefore  these patterns may  be useful for helping 
you to get familiar with a FLOSS system that is completely new for you. 
The intents of the patterns that comprise the \textit{First Contact} cluster \cite{demeyer2008} are listed in the Appendix.

\textit{Look for Todo Lists} and \textit{Easy Tasks First} are patterns 
that can be useful when you are already getting familiar with the FLOSS 
but still not sure about where you should start to contribute.

\input{../involvement/LookForTodoLists.ltx}
\input{../involvement/EasyTasksFirst.ltx}

%---------------------------
\section{\textit{Contribution Patterns}}
\label{contribution/AddTestsThatFail} \label{contribution/CreatePatch}
\label{contribution/DocumentYourChanges} \label{contribution/ExplanatoryCommitMessages}
\label{contribution/GroupRelatedChanges} \label{contribution/ManualTesting}
\label{contribution/ReviewRecentActivity} \label{contribution/ReviewYourChanges}
\label{contribution/RightVersionForTheTask} \label{contribution/SeparateUnrelatedChanges}
\label{contribution/WriteDocumentation} \label{contribution/WriteUsefulBugReports}

%NEW PREAMBLE. TO BE IMPROVED.
Your desire to contribute to a FLOSS project is well underway. 
You selected a project, you  became familiar with the legacy OS system 
and you even identified possible starting points for contributing.
How can you be sure that you will provide useful contributions,
and that they will be accepted by project leaders? 
And what about your reputation if your contributions
provide some undesirable effects to the project?
How can you start with tasks that are not only related to coding or fixing bugs?

\subsection{Forces}
\begin{itemize}
\item 
\end{itemize}

\textit{Contribution Patterns} document best practices for submitting different
kinds of contribution to a FLOSS project. At the moment,
the \textit{Contribution Patterns} are work in progress. 

In this section, we provide a brief list of the patterns and their intents.
Figure \ref{fig:contribution} presents the patterns in this cluster.

\begin{figure}[hbt]
  \begin{center}
    \includegraphics[width=\textwidth]{figures/contribution}
  \end{center}
  \caption{Contribution Patterns}
  \label{fig:contribution}
\end{figure}

\begin{itemize}
  \item \textbf{Add Tests That Fail}.
    Reproduce bugs in a automated way so that they don't come back
    later.
  \item \textbf{Manual Testing}.
    Test features by manually executing the software.
  \item \textbf{Create a Patch}.
    Produce a file that fully describes your changes to the source code
    of the project.
  \item \textbf{Explanatory Commit Messages}.
    Make it easier for other people to understand the reasons for a
    change.
  \item \textbf{Group Related Changes}.
    Submit related changes as a single patch.
  \item \textbf{Separate Unrelated Changes}.
    Submit unrelated changes in separate patches to ease the review.
  \item \textbf{Document Your Changes}.
    Update relevant documentation when adding new features or modifying
    existing ones.
  \item \textbf{Review Your Changes}.
    Put yourself in the project leaders shoes, and review your own work.
  \item \textbf{Right Version for the Task}.
    Bug fixes in stable versions, new features in the development version.
  \item \textbf{Write Documentation}.
    Contribute to a free software project by writing useful
    documentation.
  \item \textbf{Write Useful Bug Reports}.
    Provide all the information needed for other people to reproduce the
    bug you found.
  \item \textbf{Review Recent Activity}.
    Identify news and opportunities related to the software project.
\end{itemize}

\section{Conclusions}

Although free software projects tend to be open to participation, it is not
always easy to find a suitable project and get involved in the community. This
set of patterns should help potential contributors by documenting in a
structured format some useful advices and best practices from free software
communities. 

% TODO: the FSP should suit various situations: people who want to translate,
% document, to use and extend software in a corporation, hobbysts who want to
% learn a new programming language, users who want to improve the software they
% use and get involved in the community...

As future work, we intend to extend this catalog by expanding the
\textit{Contribution Patterns}. Also, we intend to refine all patterns with the
experience gained from applying the \textit{Free Software Patterns} in an
practical course on free software development.

The \textit{Free Software Patterns} catalog is itself an open project. Its
license allows free reuse of the text, as long as modified versions are made
available under the same license. Anyone interested in sharing experiences and
points of view is welcome to contribute with new patterns and refinments by
visiting the official pattern repository\footnote{Source available at
\url{https://gitorious.org/flosspapers/free-software-patterns/}.}.

\appendix

\section{\textit{First Contact}, from Object-Oriented Reengineering Patterns}
\label{involvement/ChatWithMaintainers}
\label{involvement/DoAMockInstallation}
\label{involvement/InterviewDuringDemo}
\label{involvement/ReadTheCodeInOneHour}
\label{involvement/SkimTheDocumentation}

The following patterns are part of the book ``Object-Oriented
Reengineering Patterns'' \cite{demeyer2008}:

\begin{itemize}
  \item \textbf{Chat with the Maintainers}.
    Learn about the historical and political context of your project
    through discussions with the people maintaining the system.
  \item \textbf{Do a Mock Installation}.
    Check whether you have the necessary artefacts available by
    installing the system and recompiling the code.
  \item \textbf{Interview During Demo}.
    Obtain an initial feeling for the appreciated functionality of a
    software system by seeing a demo and interviewing the person giving
    the demo.
  \item \textbf{Read the Code in One Hour}.
    Assess the state of a software system by means of a brief, but
    intensive code review.
  \item \textbf{Skim the Documentation}.
    Assess the relevance of the documentation by reading it in a limited
    amount of time.
\end{itemize}

\section*{Acknowledgements.}
We are grateful to
Demeyer, Ducasse and Nierstrasz for having licensed their work under a
Creative Commons license, allowing us to partially base our own work on
theirs.

\bibliographystyle{sbc}
\bibliography{fsp2012}

\end{document}
