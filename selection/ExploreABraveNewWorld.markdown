# Explore a Brave New World

**Intent:** Choose a project based on the functionalities it provides, despite
the risk of having to deal with unknown technologies.

## Problem

You want to select a FLOSS project to contribute with, 
and you are willing to learn any new technologies
it requires. 
Alternatively, you might be looking for a project that requires
technologies you want to learn.
You might also be looking for a project that provides a certain functionality,
and you are not deeply concerned about how it is implemented.

<!---
%Might consider asking question at end of Problem...
%for example, How do I find  project that meets the needs I have?  
%I'm having trouble seeing the problem.  You talk about it but never explicitly state it.  
-->

How do you find a FLOSS project that meets such criteria 
and it is suitable for contribution?

<!---
%Also, you rarely mention forces or tension about the problem that leads you to the solution.  
%I'd suggest you put a forces section in after problem before solution to address this.
-->

## Forces

* _There may be several projects providing the functionality you are looking for_. These projects may involve different technologies and characteristics; it may be time-consuming to select a project that fits your requirements and that is also an easy target for your contributions.

* _Making a selection based on functionality may be difficult_. The functional aspects of any software project may be complex and hard to identify -- each software may represent a new world _per se_ and the need of having at least some familiarity with its use or technologies the software uses may not be neglected. 

<!---
%Also, most solutions are weak, you need a lot more discussion.  
%You briefly talk about it but some more details are definitely needed.
-->

## Solution

When looking for a project, do not consider the technology it requires as the main criteria for selection. Instead, you can focus on other aspects:

* Does the project satisfy your needs in terms of functionality?

* If you want to integrate the project in a larger solution, how does it
  support that? For example, if you are looking for a server software, does it
  have a client library available for the language in which your application is
  written?

* Are the dependencies well documented?

<!---
%Concerning the Tradeoffs, i think there are more.  
%If you had forces you would see more as you address them.  
%For example, in the first pattern, Explore a Brave New World, you have the tradeoff 
% of difficulty of finding a solution.  You have a pro of learning a new thing or new technology.  
%I'm sure there are more.  Look at this for all patterns.
-->

## Trade Offs

**Pros:** 

* You have a higher chance of finding a project that fulfills your requirements in terms of functionality.

* You will be probably learning a new language or technology.

**Cons:** 

* You might end up spending a large amount of effort to get yourself up to speed with the technology used in the project.

* You may have a hard time to find a project, since the functional aspects of any software project may be complex and hard to identify.

## Rationale

If your choice is not influenced by technology, you can focus on functionality.

## Known Uses

[Noosfero](http://noosfero.org) is written in Ruby with some parts of the user
interface written in Javascript, and everyone in the team was confortable with
both Ruby and Javascript. When faced with the task of selecting a XMPP chat
server for Noosfero, however, the team did not find an alternative in Ruby.
[Ejabberd](http://www.ejabberd.im/) was a highly used XMPP server, but it was
written in Erlang, a language that no one in the team had experience with. In
terms of functionality, and the needed libraries for connecting to the server
from both Ruby and Javascript were available. Ejabberd fulfilled all the
needs, in terms of functionality, but for debugging some problems it was
required to understand some Erlang code.

[Analizo](http://analizo.org/) is written in Perl and needed a source code
parser. When looking into existing source code parsers, we have found that
[Doxygen](http://doxygen.org/), a documentation system for software projects,
already supported parsing C, C++ and Java. Even though Doxygen was written in
C++, it was used as a base for the source code parser used in Analizo, which
was called Doxyparse. Creating Doxyparse required re-learning the venerable art
of C++ programming.

<!---
For related patterns I think you can relate more of your patterns.
-->

## Related Patterns

It might be easier, in terms of both effort and complexity, to [Walk on
Familiar Ground](link://selection/WalkOnFamiliarGround). 
You may also favor, besides functionality, 
familiarity, when you [Look Inside Your Toy Box](link://selection/LookInsideYourToyBox).

## What Next

Since you do not necessarily know or use all the technology the new project
requires, you might want start by [Doing a Mock
Installation](link://involvement/DoAMockInstallation) to make sure you have
everything you need to get started with the code.
