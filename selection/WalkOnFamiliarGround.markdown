# Walk On Familiar Ground

**Intent:** Choose a project based on the technologies it uses that stems as a
familiar ground for you.

## Problem

You want to adopt a free software project without having to introduce unknown
technologies. Not being able to introduce new technologies might happen for a
variety of reasons:

* You are looking for a project to be incorporated in an existing solution, for
  example, a library to be used by an existing application. In this case, you
  need the library to be written in the same programming language as the
  application.

* You do not wish or have the time to learn new things. For example, your
  company has to add new functionality based on an existing project, but cannot
  afford the time required to get up to speed with new and unknown technologies
  and tools.

* Your organization has a strict policy for approving any new technology before
  it can be introduced, and you would rather avoid the trouble.

* The adoption of a new technology might impose prohibitive costs. For example,
  adopting an interpreted programming language for embedded software
  development will require adding extra storage capacity to devices in order to
  install the language interpreter.

## Solution

Look for projects that match your criteria for acceptable technology.

If your criteria includes a specific programming language, you might start by
looking for projects in language-specific repositories. For example, most Perl,
Ruby and Python projects are listed in [CPAN](http://www.cpan.org/),
[Rubygems](http://rubygems.org/) and [PyPI](http://pypi.python.org/), their
respective community repositories.

Sometimes your criteria will not be the programming language, but other parts
of an application architecture, such as a database system. For example, you
will want to check whether that new content management system you are
evaluating supports the relational database system approved by your
organization.

## Trade Offs

**Pros:** The learning curve is mimized since you are dealing with known
technologies or tools.

**Cons:** When you fixate technology criteria, you might exclude projects that
would be a better fit in terms of functionality.

## Rationale

Applying some technology requires less effort when you are already used to it
than when you have to learn it first.

## Known Uses

Back in 2006, we started working on a model transformation research project
called TEMA.  At that time, the majority of software technology for model
transformation was in Java, but the developer involved in the project preferred
Ruby. He looked for model transformation projects in Ruby, and found
[RMOF](http://rubyforge.org/projects/rmof/). The project ended up contributing
a substantial amount of code to the RMOF project.

## Related Patterns

Walking on Familiar Ground might be easier than [Exploring a Brave New
World](link://selection/ExploreABraveNewWorld).

## What Next

After choosing a project using technology-based criteria, you should probably
[Do a Mock Installation](link://involvement/DoAMockInstallation) to reaffirm
your certainty that the technology is under control.
