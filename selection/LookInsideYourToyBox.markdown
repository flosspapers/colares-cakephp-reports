# Look Inside Your Toy Box

**Intent:** Choose a project that you already use.

## Problem

If you are a Computer Science student, contributing to a free software project
may develop or improve several relevant skills related to software development
practice, including programming, bug fixing, documenting, testing, among
others. However, you may have no clue about which project to contribute to,
and no specific technology or functionality requirements.

## Solution

Contribute to a project you already use. That may be, for example, your text
editor, your web browser, your presentation software, your e-mail program, your
instant messaging program. If you already do software development, you might
want to contribute to one of the libraries you use.

## Trade Offs

**Pros:** It might be easier to spot opportunities for contributing to a
project that you already use. The contribution can be a piece of documentation
that is missing, a feature that you need, or a fix for an annoying bug.

**Cons:** By restricting yourself to projects that you already use, you may be
missing the opportunity to contribute to projects that are friendlier to
external contributors.

## Rationale

Developers "scratching their own itches" tend to be more motivated than those
working on stuff that is going to be used by other people.

## Known Uses

[phpLDAPadmin](http://phpldapadmin.sourceforge.net/), a web-based LDAP browser
and manager, was used to search and create user accounts by filling in LDAP
attributes. However, we needed a way to create accounts with predefined
attributes, according to predetermined user types. Instead of developing
specialized software for this task, we used our knowledge as phpLDAPadmin users
in order to adapt it to our needs, therefore contributing to the project.

## Related Patterns

The project you already use may very well be a [familiar
ground](link://selection/WalkOnFamiliarGround) for you, but it will more
probably lead you into a [brave new
world](link://selection/ExploreABraveNewWorld).

## What Next

As a user, you may have installed the software by using an installer or a
binary package. Therefore, your environment might lack dependencies needed to
develop the software (e.g., compilers and libraries). In this case, [Do a Mock
Installation](link://involvement/DoAMockInstallation) to make sure you have
everything you need to get started with the code.

