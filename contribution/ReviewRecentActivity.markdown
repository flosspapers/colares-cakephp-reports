# Review Recent Activity

**Intent:** Identify news and opportunities related to the software project.

## Problem



## Solution

TODO Commit mailing lists, blogs, etc.


## Rationale

A Free Software project is a collective effort, but sometimes it is difficult
to coordinate the effort of people with different schedules and priorities.
Keeping yourself up to date with what is happening in the project will help to
avoid duplicating effort and identifying opportunities for specific
collaborations.

## Example


## Known Uses



## Related Patterns



## What Next


