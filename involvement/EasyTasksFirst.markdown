# Easy Tasks First

**Intent:** Start working on tasks that can provide instant gratification.

## Problem

Starting to contribute to a project can be a daunting task. Large projects are
difficult to understand in its entirety, and new contributors might be tempted
to first understand everything and only then start contributing.

This problem is difficult because:

* By the time a new contributor understands everything about a project --
  assume that is possible! -- she might have already lost interest.

Yet it is solvable because:

* Nobody needs to understand a software system completely to make modifications
  on it. Actually, even in professional software maintenance teams it is very
  common that no single person understands the entire system.
* Contributors can build their understanding of the system incrementally while
  they contribute.

## Solution

Look for lists of easy tasks that need to be done. Such tasks include working
on bugs that have trivial fixes, adjusting the style of the source code to
match the project code style, or other housekeeping tasks.

## Trade Offs

**Pros:** Working on easy tasks allows a contributor to gain knowledge about
the system incrementally and at the same provide valuable contributions to the
project.  Besides, dealing with easy tasks allows the new contributor to focus
on the process and not only on the task.

**Cons:** the contributor might feel that the easy tasks are not important
enough because nobody did them before. However, it is useful
to consider that dealing with the hard stuff may leave no room for thinking about the easy ones; also, sometimes small changes have great positive impacts on the project and on its users.

## Rationale

Besides knowing the code, there is also a lot of other aspects of a free
software project that a developer can get used to by working on "trivial"
tasks: getting used to the procedure for submitting contributions and having
them reviewed by more experienced project members, interacting with the project
infrastructure (bug tracking systems, communication channels etc) and others.
By the time the new contributor has acquired the expertise to work on "more
relevant" tasks (that are by definition also more difficult), the supporting
infrastructure will not be an issue anymore.

## Known uses

When the [LibreOffice project](http://www.libreoffice.org/) started, the
initial team wanted to attract new contributors. They created an initiative
called [Easy Hacks](http://wiki.documentfoundation.org/Development/Easy_Hacks),
that provides guidance for new contributors and lists of tasks considered easy
for newcomers to accomplish.

GNOME has a similar initiative called [GNOME
Love](https://live.gnome.org/GnomeLove), which is "the place to learn how to
start contributing to GNOME". Besides providing guidance on the topics
contributors need to learn, GNOME Love also includes marking bugs that should
be easy for new contributors to solve.

Noosfero has a list of tasks marked as [Easy to
Solve](http://noosfero.org/Development/EasyToSolve) where prospective
developers can find bite-sized bugs to fix or enhancements to make.

## Related Patterns

You might want to [Look for TODO Lists](link://involvement/LookForTodoLists)
that already contain tasks categorized by difficulty level.

## What Next

After selecting easy tasks to start, you might want to look at some
contribution patterns. Having the [Right Version for the
Task](link://contribution/RightVersionForTheTask) is crucial, and make sure you
[Review Your Changes](link://contribution/ReviewYourChanges) before submitting
them.
