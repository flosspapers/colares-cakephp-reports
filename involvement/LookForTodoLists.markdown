# Look for TODO Lists

**Intent:** Discover places/things that may be good starting points for
contributing to a project.

## Problem

You are willing to contribute but has no idea of what to do or where to start.
Due to your low experience with the project, you want to avoid working on high
priority tasks. You may be worried that you will not be able to complete the
task in a timely manner, which can lead to one of two problems: you may prevent
other developers from doing their work, or you may discover that another
developer completed the task before you, due to its high priority.

## Solution

Look for TODO lists in the source code of the project. Many projects have TODO
lists, i.e., lists of things that should be done eventually. These are usually
low priority tasks, or tasks that no one has taken the time to think about
seriously.

TODO lists come in two flavors:

* explicit TODO lists in a document (for example, a file named `TODO` in the
  project's root folder) that is bundled together with the application source
  code;
* comments in the source code, marked by words such as `TODO`, `FIXME`, `XXX`,
  and `???`.

Explicit TODO lists are usually plain text, itemized lists of tasks. Sometimes
they are organized hierarchically, with items and subitems.

Most source code editors highlight occurrences of at least one of the markers,
and some integrated development environments also create a list of such
occurrences. You can also use the command-line utility `grep` to look for these
markers.

The meaning for each specific marker is not well-defined. The [following
definitions](http://c2.com/cgi/wiki?FixmeComment) were described by [Martin
Pool](http://c2.com/cgi/wiki?MartinPool):

> There are useful distinctions between different tags. 'FIXME' is for things
> which are definitely broken, but where you want to not worry about it for
> the moment. 'TODO' is for useful features, optimizations or refactorings
> that might be worth doing in the future. XXX is for things that require
> more thought and that are arguably broken. Of course, you can make up your
> own categories, but I like these ones and they seem kind of standard.

He also gave some examples:

    /* TODO: How about auto-correcting small spelling 
             errors? */
    /* FIXME: This won't work if the file is missing. */
    /* XXX: This method badly needs refactoring: should switch 
            by core type. */

Source code documentation systems usually define special markers for annotating
tasks in the source code. For example, [Doxygen](http://doxygen.sf.net/)
creates a cross-referenced list with all occurrences of `\todo` or `@todo`
inside source code comments. If the project uses a specific source code
documentation system, check the system's documentation for a list of supported
markers.

## Trade Offs

**Pros:** Since source code comments usually refer to nearby code, it is easier
to understand what needs to be changed in order to resolve the issue. Also,
because source code comments usually refer to low priority, non critical tasks,
you can take your time.

**Cons:** Because source code comments and TODO files, unlike bug reports, are
not usually updated until the task is done, they might contain outdated
information (e.g., maybe someone is already working on a TODO).

## Rationale

Because TODO lists usually contain low priority tasks not assigned to any
particular developer, chances are good that no one is working on the task, so
you do not need to rush.

## Known Uses

Many projects uses either explicit TODO lists, or special source comments, or
both. For example, [JUnit](https://github.com/KentBeck/junit/) contains a file
named `to-do.txt` with a itemized, hierarchical, list of tasks, but no TODO
markers inside source code comments. On the other hand,
[Node.js](https://github.com/joyent/node) contains several `TODO`, `FIXME`, and
`XXX` comments, but no TODO file.

## Related Patterns

If you are not familiar with the project, you should try [Easy Tasks
First](link://involvement/EasyTasksFirst). It might be a good idea to [Review
Recent Activity](link://contribution/ReviewRecentActivity) to make sure your
contribution is still relevant.

## What Next

After finding a suitable contribution, it should be useful to read some
Contribution Patterns. Be sure that you have the [Right Version for the
Task](link://contribution/RightVersionForTheTask), and [Review Your
Changes](link://contribution/ReviewYourChanges) before committing them.
